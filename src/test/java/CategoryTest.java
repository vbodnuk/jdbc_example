import connect.ConnectToDB;
import dao.CategoryDao;
import dao.impl.CategoryDaoImpl;
import entity.Category;
import org.junit.*;


import java.sql.SQLException;



public class CategoryTest {
    private  ConnectToDB connectToDB;
    private  CategoryDao category;


    @Before
    public  void connection() throws SQLException {
        connectToDB = new ConnectToDB();
        connectToDB.getConnection().setAutoCommit(false);
        category = new CategoryDaoImpl();
    }

    @After
    public  void rollback() throws SQLException {
        connectToDB.getConnection().rollback();
        connectToDB.getConnection().setAutoCommit(true);
        connectToDB.getConnection().close();
    }

    @Test
    public void create() throws SQLException {
        Category c = new Category();
        c.setName("name");
        Assert.assertTrue(category.create(c));
    }

    @Test
    public void read() throws SQLException {
        Assert.assertNotNull(category.read(1));
    }

    @Test
    public void readAll() throws SQLException {
        Assert.assertNotNull(category.readAll());
    }

    @Test
    public void update() throws SQLException {
        Assert.assertTrue(category.update("name",2));
    }

    @Test
    public void delete() throws SQLException{
        Assert.assertTrue(category.delete(1));

    }

}
