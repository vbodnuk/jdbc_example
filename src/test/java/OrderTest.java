import connect.ConnectToDB;
import dao.OrderDao;
import dao.impl.OrderDaoImpl;
import entity.Order;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class OrderTest {
    private ConnectToDB connectToDB;
    private OrderDao order;

    @Before
    public  void connection() throws SQLException {
        connectToDB = new ConnectToDB();
        connectToDB.getConnection().setAutoCommit(false);
        order = new OrderDaoImpl();
    }

    @After
    public  void rollback() throws SQLException {
        connectToDB.getConnection().rollback();
        connectToDB.getConnection().setAutoCommit(true);
        connectToDB.getConnection().close();
    }

    @Test
    public void create() throws SQLException {
        Order o  = new Order();
        o.setDate("12.08.2017");
        o.setStatus("NEW");
        o.setUser_id(4);
        Assert.assertTrue(order.create(o));
    }

    @Test
    public void read() throws SQLException {
        Assert.assertNotNull(order.read(1));
    }

    @Test
    public void readAll() throws SQLException {
        Assert.assertNotNull(order.readAll());
    }

    @Test
    public void update() throws SQLException {
        Assert.assertTrue(order.update("Old",2));
    }

    @Test
    public void delete() throws SQLException{
        Assert.assertTrue(order.delete(1));

    }

}
