import connect.ConnectToDB;
import dao.OrderProductDao;
import dao.impl.OrderProductDaoImpl;
import entity.OrderProduct;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class OrderProductTest {
    private ConnectToDB connectToDB;
    private OrderProductDao order;

    @Before
    public  void connection() throws SQLException {
        connectToDB = new ConnectToDB();
        connectToDB.getConnection().setAutoCommit(false);
        order = new OrderProductDaoImpl();
    }

    @After
    public  void rollback() throws SQLException {
        connectToDB.getConnection().rollback();
        connectToDB.getConnection().setAutoCommit(true);
        connectToDB.getConnection().close();
    }

    @Test
    public void create() throws SQLException {
        OrderProduct o  = new OrderProduct();
        o.setOrder_id(3);
        o.setProduct_id(4);
        Assert.assertTrue(order.create(o));
    }

    @Test
    public void read() throws SQLException {
        Assert.assertNotNull(order.read(1));
    }

    @Test
    public void readAll() throws SQLException {
        Assert.assertNotNull(order.readAll());
    }

    @Test
    public void update() throws SQLException {
        Assert.assertTrue(order.update(3,2));
    }

    @Test
    public void delete() throws SQLException{
        Assert.assertTrue(order.delete(1));

    }
}
