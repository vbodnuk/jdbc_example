package dao;

import entity.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {
    public boolean create(User user) throws SQLException;
    public User read(int i) throws SQLException;
    public List<User> readAll() throws SQLException;
    public boolean update(String s, int i) throws SQLException;
    public boolean delete(int i) throws SQLException;
}
