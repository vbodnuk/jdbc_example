package dao;

import entity.OrderProduct;

import java.sql.SQLException;
import java.util.List;

public interface OrderProductDao {
    public boolean create(OrderProduct orderProduct) throws SQLException;
    public OrderProduct read(int i) throws SQLException;
    public List<OrderProduct> readAll() throws SQLException;
    public boolean update(int i, int j) throws SQLException;
    public boolean delete(int i) throws SQLException;
}
