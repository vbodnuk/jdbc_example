package dao;

import entity.Order;

import java.sql.SQLException;
import java.util.List;

public interface OrderDao {
    public boolean create(Order order) throws SQLException;
    public Order read(int i) throws SQLException;
    public List<Order> readAll() throws SQLException;
    public boolean update(String s, int i) throws SQLException;
    public boolean delete(int i) throws SQLException;
}
