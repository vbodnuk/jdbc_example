package dao;

import entity.Category;

import java.sql.SQLException;
import java.util.List;

public interface CategoryDao {
    public boolean create(Category category) throws SQLException;
    public Category read(int i) throws SQLException;
    public List<Category> readAll() throws SQLException;
    public boolean update(String s,int i) throws SQLException;
    public boolean delete(int i) throws SQLException;

}
