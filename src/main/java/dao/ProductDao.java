package dao;

import entity.Product;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

public interface ProductDao {
    public boolean create(Product product) throws SQLException;
    public Product read(int i) throws SQLException;
    public List<Product> readAll() throws SQLException;
    public boolean update(BigDecimal bd, int i) throws SQLException;
    public boolean delete(int i) throws SQLException;
}
