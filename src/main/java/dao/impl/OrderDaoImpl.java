package dao.impl;

import connect.ConnectToDB;
import dao.OrderDao;
import entity.Order;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OrderDaoImpl implements OrderDao {

    @Override
    public boolean create(Order order) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "insert into Order(date_created, user_id, status)" + "value(?,?,?);";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setString(1, order.getDate());
            ps.setInt(2, order.getUser_id());
            ps.setString(3, order.getStatus());
            ps.executeUpdate();
            b = true;
            System.out.println("Added successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;

    }

    @Override
    public Order read(int id) throws SQLException {
        ConnectToDB connectToDB = new ConnectToDB();
        Order order = new Order();
        String query = "select * from Order where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                order.setId(resultSet.getInt("id"));
                order.setDate(resultSet.getString("date_created"));
                order.setStatus(resultSet.getString("status"));
                order.setUser_id(resultSet.getInt("user_id"));
            }
            System.out.println(order);
            return order;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return null;
    }

    @Override
    public List<Order> readAll() throws SQLException {
        ConnectToDB connectToDB = new ConnectToDB();
        List<Order> orders = new ArrayList<>();
        String query = "select * from Order;";
        Statement statement = null;
        try {
            statement = connectToDB.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("id"));
                order.setDate(resultSet.getString("date_created"));
                order.setStatus(resultSet.getString("status"));
                order.setUser_id(resultSet.getInt("user_id"));
                orders.add(order);
                System.out.println(order);
            }
            return orders;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(statement != null){
                statement.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return null;

    }

    @Override
    public boolean update(String setStatus, int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "update Order set status = ? where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setString(1, setStatus);
            ps.setInt(2, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Updated successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "delete from Order where id = ?";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Deleted successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }
}
