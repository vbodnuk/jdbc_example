package dao.impl;

import connect.ConnectToDB;
import dao.ProductDao;
import entity.Product;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class ProductDaoImpl implements ProductDao {

    @Override
    public boolean create(Product product) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "insert into Product(name, price, category_id)" + "value(?,?,?);";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setString(1, product.getName());
            ps.setBigDecimal(2, product.getPrice());
            ps.setInt(3, product.getCategory_id());
            ps.executeUpdate();
            b = true;
            System.out.println("Added successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }

    @Override
    public Product read(int id) throws SQLException {
        Product product = new Product();
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "select * from Product where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                product.setId(resultSet.getInt("id"));
                product.setName(resultSet.getString("name"));
                product.setPrice(resultSet.getBigDecimal("price"));
                product.setCategory_id(resultSet.getInt("category_id"));
            }
            System.out.println(product);
            return product;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return  null;
    }

    @Override
    public List<Product> readAll() throws SQLException {
        List<Product> products = new ArrayList<>();
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "select * from Product;";
        Statement statement = null;
        try {
            statement = connectToDB.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Product product = new Product();
                product.setId(resultSet.getInt("id"));
                product.setName(resultSet.getString("name"));
                product.setPrice(resultSet.getBigDecimal("price"));
                product.setCategory_id(resultSet.getInt("category_id"));
                products.add(product);
                System.out.println(product);
            }
            return products;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(statement != null){
                statement.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return null;
    }

    @Override
    public boolean update(BigDecimal price ,int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "update Product set price = ? where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setBigDecimal(1, price);
            ps.setInt(2, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Updated successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "delete from Product where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Deleted successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }
}
