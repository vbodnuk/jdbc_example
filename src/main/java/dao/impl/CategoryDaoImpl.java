package dao.impl;

import connect.ConnectToDB;
import dao.CategoryDao;
import entity.Category;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class CategoryDaoImpl implements CategoryDao{

    @Override
    public boolean create(Category category) throws SQLException {
        boolean b ;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "insert into Category(name)" + "values(?);";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setString(1, category.getName());
            ps.executeUpdate();
            b = true;
            System.out.println("Added successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if (ps != null) {
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }

        return b;
    }



    @Override
    public Category read(int id) throws SQLException {
        Category category = new Category();
        ConnectToDB connectToDB = new ConnectToDB();
        String query ="select * from Category where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                category.setId(rs.getInt("id"));
                category.setName(rs.getString("name"));
            }
            System.out.println(category);
            return category;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null ){
                connectToDB.getConnection().close();
            }
        }
        return null;
    }

    @Override
    public List<Category> readAll() throws SQLException {
        List<Category> categories = new ArrayList<>();
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "select * from Category;";
        Statement statement = null;
        try {
            statement = connectToDB.getConnection().createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Category category = new Category();
                category.setId(rs.getInt("id"));
                category.setName(rs.getString("name"));
                categories.add(category);
                System.out.println(category);
            }
            return categories;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(statement != null){
                statement.close();
            }
            if(connectToDB.getConnection() != null ){
                connectToDB.getConnection().close();
            }
        }

        return null;
    }

    @Override
    public boolean update(String setName, int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "update Category set name = ? where id = ?";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setString(1, setName);
            ps.setInt(2, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Updated successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null ){
                connectToDB.getConnection().close();
            }
        }
        return b;

    }

    @Override
    public boolean delete(int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "delete c.*,p.* from Category c inner join Product p " +
                "on c.id = p.category_id INNER JOIN Order_product op on p.id = op.product_id " +
                "where c.id = ?";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Deleted successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null ){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }

}
