package dao.impl;

import connect.ConnectToDB;
import dao.OrderProductDao;
import entity.OrderProduct;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class OrderProductDaoImpl implements OrderProductDao {

    @Override
    public boolean create(OrderProduct orderProduct) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "insert into Order_product(order_id,product_id)" + "value(?,?);";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, orderProduct.getOrder_id());
            ps.setInt(2, orderProduct.getProduct_id());
            ps.executeUpdate();
            b = true;
            System.out.println("Added successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }

    @Override
    public OrderProduct read(int id) throws SQLException {
        OrderProduct orderProduct = new OrderProduct();
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "select * from Order_product where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                orderProduct.setId(resultSet.getInt("id"));
                orderProduct.setOrder_id(resultSet.getInt("order_id"));
                orderProduct.setProduct_id(resultSet.getInt("product_id"));
            }
            System.out.println(orderProduct);
            return orderProduct;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return null;
    }

    @Override
    public List<OrderProduct> readAll() throws SQLException {
        List<OrderProduct> products= new ArrayList<>();
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "select * from Order_product;";
        Statement statement = null;
        try {
            statement = connectToDB.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                OrderProduct orderProduct = new OrderProduct();
                orderProduct.setId(resultSet.getInt("id"));
                orderProduct.setOrder_id(resultSet.getInt("order_id"));
                orderProduct.setProduct_id(resultSet.getInt("product_id"));
                products.add(orderProduct);
                System.out.println(orderProduct);
            }
            return products;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(statement != null){
                statement.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return null;
    }


    @Override
    public boolean update(int setOrderId,int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "update Order_product set order_id = ? where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, setOrderId);
            ps.setInt(2, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Updated successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "delete from Order_product where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Deleted successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }
}
