package dao.impl;

import connect.ConnectToDB;
import dao.UserDao;
import entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class UserDaoImpl implements UserDao {

    @Override
    public boolean create(User user) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "insert into User(email, password, lang, age)" + " values(?,?,?,?);";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setString(1, user.getEmail());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getLang());
            ps.setInt(4, user.getAge());
            ps.executeUpdate();
            b = true;
            System.out.println("Added successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }

    @Override
    public User read(int id) throws SQLException {
        User user = new User();
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "select * from User where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                user.setAge(resultSet.getInt("age"));
                user.setId(resultSet.getInt("id"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setLang(resultSet.getString("lang"));
            }
            System.out.println(user);
            return user;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return null;
    }

    @Override
    public List<User> readAll() throws SQLException {
        List<User> users = new ArrayList<>();
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "select * from User;";
        Statement statement = null;
        try {
            statement = connectToDB.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                User user = new User();
                user.setAge(resultSet.getInt("age"));
                user.setId(resultSet.getInt("id"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setLang(resultSet.getString("lang"));
                users.add(user);
                System.out.println(user);
            }
            return users;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(statement != null){
                statement.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return null;
    }

    @Override
    public boolean update(String lang, int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "update User set lang = ? where id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setString(1, lang);
            ps.setInt(2, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Updated successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }
        }
        return b;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        boolean b;
        ConnectToDB connectToDB = new ConnectToDB();
        String query = "delete from User where id = ?;";
        PreparedStatement ps = null;
        if(ps != null){
            ps.close();
        }
        try {
            ps = connectToDB.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
            b = true;
            System.out.println("Deleted successful");
        }catch (SQLException e){
            e.printStackTrace();
            b = false;
        }finally {
            if(ps != null){
                ps.close();
            }
            if(connectToDB.getConnection() != null){
                connectToDB.getConnection().close();
            }

        }
        return b;
    }
}
