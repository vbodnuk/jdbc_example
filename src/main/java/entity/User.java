package entity;


public class User {
    public String email;
    public int id;
    public String password;
    public int age;
    public String lang;

    public User() {

    }

    public User(String email, int id, String password, int age, String lang) {
        this.email = email;
        this.id = id;
        this.password = password;
        this.age = age;
        this.lang = lang;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public String toString() {
        return  email + " " + id + " " + password + " " + age + " " + lang ;
    }
}
