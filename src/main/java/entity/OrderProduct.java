package entity;


public class OrderProduct {
    public int id;
    public int order_id;
    public int product_id;

    public OrderProduct (){}

    public OrderProduct(int id, int order_id, int product_id) {
        this.id = id;
        this.order_id = order_id;
        this.product_id = product_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    @Override
    public String toString() {
        return id + "," + order_id + "," + product_id;
    }
}
