package entity;


public class Order {

    public int id;
    public String date;
    public int user_id;
    public String status;

    public Order (){}

    public Order(int id, String date,  int user_id) {
        this.id = id;
        this.date = date;

        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return id + "," + date + "," + status + "," + user_id;
    }



}
