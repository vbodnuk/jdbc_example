package entity;


import java.math.BigDecimal;

public class Product {
    public int id;
    public String name;
    public BigDecimal price;
    public int category;

    public Product(){}

    public Product(int id, String name, BigDecimal price, int category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getCategory_id() {
        return category;
    }

    public void setCategory_id(int category_id) {
        this.category = category_id;
    }

    @Override
    public String toString() {
        return id+ " " + name + " " + price + " " + category;
    }
}
