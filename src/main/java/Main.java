
import dao.impl.CategoryDaoImpl;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        CategoryDaoImpl categoryDao = new CategoryDaoImpl();
        categoryDao.readAll();
    }
}
